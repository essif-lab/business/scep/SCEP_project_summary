# SCEP Project Summary

For more information, please contact: Pavel Metelitsyn, pavel@domilabs.io


## Introduction

### About Domi Labs

Domi Labs was founded 2019 in Berlin. The main focus of the company is the research of possibilities of SSI for the digitization of business processes in the real estate world. SCEP is an attempt to generalise our findings and to create a generic SSI-enabled document automation and contractual event tracking platform.

### SCEP

We aim to build an SSI-enabled electronic contracting solution which allows credentialisation of legal contracts and contractual events. This includes providing verifiable evidence of fulfilment of contractual obligations, for example, or any other kind of verifiable selective disclosure of contract details or content in order to build trust with third parties outside of the contract, such as potential creditors, regulators, business counterparties, etc.

We are building a solution that:
1. generates, handles, and verifies contracts that are machine readable and tamper-proof, while still being legally binding across EU member states,
2. encapsulates the full lifecycle of a contract between two or more parties,
3. provides a mechanism for linking real world events to pre-existing contracts, allowing individuals or legal persons to maintain an SSI-capable “passport” of such events.

## Summary

### Business Problem

Existing popular digital contract solutions like DocuSign, HelloSign and others typically provide a convenient API/interface to seal a PDF document but have serious technical and business limitations. For one, they only establish authenticity of the sealed document, effectively providing no substantial advantage over digitally self-signed documents aside from convenience and user experience. Such solutions are limited in their ability to address a number of business requirements for electronic contracts such as identifying and verifying signing parties, non-repudiation, change management, and generating audit trails of execution.

For such reasons, businesses dealing with electronic contracts often have to invent their own proprietary solutions, patching different standards and software components together in a not-standardized, non-interoperable way.

We propose a solution based on SSI and related standards which addresses these issues allowing for streamlined, interoperable electronic contracting. Our solution will enable businesses to transition easily to natively digital contracts. Our solution enables better management of both internal and external customer relations, as the easily shared and exported "passport" of contractual events enables both parties access to a shared verified record of their interactions, saving significant cost, time and stress.

### Technical Solution

Our solution is intended to work in tandem with an existing/third-party edge wallet or cloud-based SSI-Wallet. It will be available as 
1) a software library (SDK) that implements necessary core data structures, business logic and protocols. This library is intended to be used by developers of edge wallets or cloud-wallet client apps, to enable their software to participate in electronic contracting, signing and storing the generated verifiable credentials securely.
2) a full-featured web service / SaaS that can be accessed via web frontend or an API for business customers and developers who want to use electronic contracting and/or integrate with their own pre-existing IT-infrastructure.

Main functionalities that will be our focus:

1) generating the contractual documents based on a verified contract template and verifiable data about the contract parties.

2) representing an effective contract as Verifable Credential containing a set of claims about the contract parties, contract terms and contractual events.

3) registering webhooks for contractual events which will trigger automated issuance of additional/supplemental credentials (i.e., "receipts").

4) integration with the broader SSI ecosystem, especially with other wallets and eSignature providers.


## Integration with the eSSIF-Lab Functional Architecture and Community

Our software platform will integrate third-party SSI-Wallets and may be integrated by third party SSI-Wallets as well as other types of services.

Regarding the eSSIF-Lab Architecture, our component is positioned as a middleware between the Business Transaction Layer and SSI Roles Layer. Particularly if the data model and semantics of the credentials it produces can be specified and/or aligned with existing standards in the regtech space, the credentials produced would not just be machine-readable but consumable by all kinds of business processes elsewhere in the SSI space.





